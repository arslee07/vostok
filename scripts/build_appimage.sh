#!/bin/sh -e

# This script builds the Linux application in AppImage format.
# To run the script, you must be in the root directory of the project,
# and appimagetool must also be installed.

BUILD="./build/linux/x64/release/bundle"
APPDIR="./build/linux/x64/release/bundle/AppDir"

if ! [ -x "$(command -v appimagetool)" ]; then
  echo "Error: appimagetool is not installed." >&2
  echo "Download it from https://github.com/AppImage/AppImageKit/releases" >&2
  echo "And make sure that the program path is set in \$PATH" >&2
  exit 1
fi


flutter build linux || exit 1

mkdir $APPDIR
cp $BUILD/vostok $APPDIR/AppRun
cp $BUILD/lib -r $APPDIR
cp $BUILD/data -r $APPDIR
cat <<EOF > $APPDIR/Vostok.desktop
[Desktop Entry]
Name=Vostok
Exec=vostok
Icon=vostok
Type=Application
Categories=Network;
EOF
cp assets/icon.png $APPDIR/vostok.png

appimagetool $APPDIR $BUILD/Vostok.AppImage

rm -r $APPDIR

echo
echo "Done! Here's your AppImage:"
echo $BUILD/Vostok.AppImage