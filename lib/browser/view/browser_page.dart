import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:gemini_client/gemini_client.dart';
import 'package:gemtext/gemtext.dart' show Gemtext;
import 'package:url_launcher/url_launcher.dart';
import 'package:vostok/browser/browser.dart';

final uriRegex = RegExp(
  r'^([A-Za-z0-9]+:(\/\/)?)?(([A-Za-z0-9\-:]+[@\.])*([A-Za-z0-9\-]+\.[A-Za-z0-9\-]+|localhost)(:?\d+)?\/?).*$',
);
final schemeRegex = RegExp(
  r'^[A-Za-z0-9]+:(\/\/)?',
);

Uri _parse(String input) {
  Uri uri;

  if (uriRegex.hasMatch(input.trim())) {
    if (schemeRegex.hasMatch(input)) {
      uri = Uri.parse(input);
    } else {
      uri = Uri.parse('gemini://$input');
    }
  } else {
    uri = Uri.parse(
      'gemini://geminispace.info/search?${Uri.encodeComponent(input)}',
    );
  }

  if (uri.hasEmptyPath) {
    uri = uri.replace(path: '/');
  }

  return uri;
}

Uri _prepareUrl(Uri url, Uri newUrl) {
  if (newUrl.isAbsolute) return newUrl;

  Uri res;

  if (newUrl.hasAbsolutePath) {
    res = url.replace(path: newUrl.path, query: newUrl.query);
  } else {
    res = url.replace(path: url.path + newUrl.path, query: newUrl.query);
  }

  // a workaround to remove trailing question mark
  final strRes = res.toString();
  if (strRes.endsWith('?')) {
    res = Uri.parse(strRes.substring(0, strRes.length - 1));
  }

  return res;
}

class BrowserPage extends StatelessWidget {
  final String input;

  const BrowserPage(this.input, {super.key});

  @override
  Widget build(BuildContext context) {
    final url = _parse(input);

    return Scaffold(
      appBar: AppBar(title: Text('$url', overflow: TextOverflow.fade)),
      body: FutureBuilder<Response>(
        future: GeminiClient().send(host: url.host, request: Request(url: url)),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final data = snapshot.data!;
            final statusCode = data.statusCode;

            if (!StatusCode.isKnown(statusCode)) {
              return const ErrorView(title: 'Unknown status code');
            } else {
              if (StatusCode.isInput(statusCode)) {
                return Input(
                  prompt: data.meta,
                  sensitive: statusCode == StatusCode.sensitiveInput,
                  onSubmit: (value) => _handleInput(url, value, context),
                );
              }

              if (StatusCode.isSuccess(statusCode)) {
                final mime = data.meta.isEmpty ? 'text/gemini' : data.meta;

                if (mime.startsWith('text/') ||
                    mime.startsWith('application')) {
                  if (mime.startsWith('text/gemini')) {
                    return GemtextViewer(
                      Gemtext.parse(utf8.decode(data.body)),
                      onUrlClick: (newUrl) => _handleLink(url, newUrl, context),
                    );
                  } else {
                    return PlainTextViewer(utf8.decode(data.body));
                  }
                } else {
                  return ErrorView(
                    title: 'Unsupported MIME-type',
                    additionalInfo: 'Got $mime MIME-type.'
                        ' Only text/* and application/* types are currently supported.',
                  );
                }
              }

              if (StatusCode.isTemporaryFailure(statusCode)) {
                return ErrorView(
                  title: 'Temporary failure',
                  additionalInfo: data.meta.isEmpty ? null : data.meta,
                );
              }

              if (StatusCode.isPermanentFailure(statusCode)) {
                return ErrorView(
                  title: 'Permanent failure',
                  additionalInfo: data.meta.isEmpty ? null : data.meta,
                );
              }

              if (StatusCode.isClientCertificateRequired(statusCode)) {
                return ErrorView(
                  title: 'Client certificate required',
                  additionalInfo: data.meta.isEmpty ? null : data.meta,
                );
              }

              return ErrorView(
                title: 'Unexpected error',
                additionalInfo:
                    'Unhandled status code $statusCode. This should not happen!\nPlease report it to https://codeberg.org/arslee07/Vostok',
              );
            }
          } else if (snapshot.hasError) {
            return ErrorView(
              title: 'Unexpected error',
              additionalInfo: snapshot.error.toString(),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  void _handleInput(Uri url, String prompt, BuildContext context) {
    final res = url.replace(query: Uri.encodeComponent(prompt));

    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (context) => BrowserPage(res.toString()),
      ),
    );
  }

  void _handleLink(Uri url, Uri newUrl, BuildContext context) {
    final res = _prepareUrl(url, newUrl);

    if (res.isScheme('gemini')) {
      Navigator.of(context).push(
        MaterialPageRoute<void>(
          builder: (context) => BrowserPage(res.toString()),
        ),
      );
    } else {
      launchUrl(newUrl);
    }
  }
}
