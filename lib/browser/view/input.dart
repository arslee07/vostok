import 'package:flutter/material.dart';

class Input extends StatelessWidget {
  final void Function(String value) onSubmit;
  final String prompt;
  final bool sensitive;

  const Input({
    required this.onSubmit,
    required this.prompt,
    required this.sensitive,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              prompt,
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(height: 16.0),
            TextField(
              decoration: const InputDecoration(
                hintText: '(enter a prompt)',
                border: OutlineInputBorder(),
              ),
              obscureText: sensitive,
              keyboardType: TextInputType.url,
              onSubmitted: onSubmit,
            ),
          ],
        ),
      ),
    );
  }
}
