import 'dart:ui';

import 'package:flutter/material.dart';

class PlainTextViewer extends StatelessWidget {
  final String document;
  final void Function(Uri url)? onUrlClick;

  const PlainTextViewer(this.document, {this.onUrlClick, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SelectableRegion(
      focusNode: FocusNode(),
      selectionControls: materialTextSelectionControls,
      child: SingleChildScrollView(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Center(
              child: Text(
                document,
                style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                  fontFeatures: const [FontFeature.tabularFigures()],
                  fontFamily: 'RobotoMono',
                  height: 0.0,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
