import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:gemtext/gemtext.dart' as gmi;

class GemtextViewer extends StatelessWidget {
  final gmi.Gemtext document;
  final void Function(Uri url)? onUrlClick;

  const GemtextViewer(this.document, {this.onUrlClick, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;

    final padding = width < 960.0
        ? const EdgeInsets.symmetric(horizontal: 12.0)
        : EdgeInsets.symmetric(horizontal: 12.0 + ((width - 960.0) / 2));

    return SelectableRegion(
      focusNode: FocusNode(),
      selectionControls: materialTextSelectionControls,
      child: ListView(
        padding: padding,
        children: [
          for (final node in document.nodes)
            Align(
              alignment: Alignment.centerLeft,
              child: switch (node) {
                gmi.Text() => TextBlock(node),
                gmi.Heading() => HeadingBlock(node),
                gmi.Link() => LinkBlock(node, onUrlClick),
                gmi.ListItem() => ListItemBlock(node),
                gmi.Preformatted() => PreformattedBlock(node),
                gmi.Quote() => QuoteBlock(node),
              },
            ),
        ],
      ),
    );
  }
}

class TextBlock extends StatelessWidget {
  final gmi.Text text;
  const TextBlock(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text.text);
  }
}

class HeadingBlock extends StatelessWidget {
  final gmi.Heading heading;
  const HeadingBlock(this.heading, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Text(
      heading.body,
      style: switch (heading.level) {
        gmi.HeadingLevel.h1 => textTheme.headlineLarge,
        gmi.HeadingLevel.h2 => textTheme.headlineMedium,
        gmi.HeadingLevel.h3 => textTheme.headlineSmall,
      },
    );
  }
}

class ListItemBlock extends StatelessWidget {
  static const bullet = '\u2022';

  final gmi.ListItem item;
  const ListItemBlock(this.item, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('$bullet ${item.body}');
  }
}

class QuoteBlock extends StatelessWidget {
  final gmi.Quote quote;
  const QuoteBlock(this.quote, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
            color: Theme.of(context).colorScheme.secondary,
            width: 2.0,
          ),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 12),
        child: Text(
          quote.body,
          style: TextStyle(color: Theme.of(context).colorScheme.secondary),
        ),
      ),
    );
  }
}

class LinkBlock extends StatelessWidget {
  final gmi.Link link;
  final void Function(Uri url)? onUrlClick;
  const LinkBlock(this.link, this.onUrlClick, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text.rich(
      TextSpan(
        text: link.name ?? link.to.toString(),
        style: Theme.of(context)
            .textTheme
            .labelLarge
            ?.copyWith(color: Theme.of(context).colorScheme.primary),
        recognizer: TapGestureRecognizer()
          ..onTap = () {
            if (onUrlClick != null) {
              onUrlClick!(link.to);
            }
          },
      ),
    );
  }
}

class PreformattedBlock extends StatelessWidget {
  final gmi.Preformatted pre;
  const PreformattedBlock(this.pre, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = ScrollController();

    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.onBackground.withOpacity(0.1),
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: Scrollbar(
        controller: controller,
        child: SingleChildScrollView(
          controller: controller,
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Text(
              pre.text,
              style: Theme.of(context).textTheme.bodySmall?.copyWith(
                fontFeatures: const [FontFeature.tabularFigures()],
                fontFamily: 'RobotoMono',
                height: 0.0,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
