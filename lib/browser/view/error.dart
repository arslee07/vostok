import 'dart:math';

import 'package:flutter/material.dart';

class ErrorView extends StatelessWidget {
  final String title;
  final String? additionalInfo;

  static const emojis = ['(μ_μ)', '(-ω-、)', '(╥_╥)', '( ; ω ; )'];

  const ErrorView({
    required this.title,
    this.additionalInfo,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              emojis[Random().nextInt(emojis.length)],
              style: Theme.of(context).textTheme.displayMedium?.copyWith(
                  color: Theme.of(context).colorScheme.primary,
                  fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 12),
            Text(title, style: Theme.of(context).textTheme.headlineMedium),
            if (additionalInfo != null) Text(additionalInfo!),
          ],
        ),
      ),
    );
  }
}
