import 'package:flutter/material.dart';
import 'package:vostok/browser/browser.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController();

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const Icon(Icons.rocket_launch_outlined),
                  const SizedBox(width: 4.0),
                  Text(
                    'Where will we go today?',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
              const SizedBox(height: 16.0),
              TextField(
                decoration: const InputDecoration(
                  hintText: '(enter an url or search query)',
                  border: OutlineInputBorder(),
                ),
                keyboardType: TextInputType.url,
                onSubmitted: (value) {
                  Navigator.of(context).push(
                    MaterialPageRoute<void>(
                      builder: (BuildContext context) => BrowserPage(value),
                    ),
                  );
                },
                controller: controller,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
